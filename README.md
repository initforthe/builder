# Initforthe Docker builder

The docker images built from this repository are used as the base for all CI builds at Initforthe.
They are also used for CI based testing of Rails projects.

Images are based on Alpine Linux to keep size down.

## Usage

The images are available from the [Gitlab Container Registry](https://gitlab.com/initforthe/builder/container_registry).

To pull an image:

```bash
docker pull registry.gitlab.com/initforthe/builder:latest
```

### Available tags

The following tags are available:

| Tag                  | Description                       | Ruby Version | Alpine Linux Version |
| -------------------- | --------------------------------- | ------------ | -------------------- |
| `3.2`, `3`, `latest` | Latest stable release of Ruby 3.2 | 3.2.x        | latest               |
| `3.1`                | Latest stable release of Ruby 3.1 | 3.1.x        | latest               |
| `3.0`                | Latest stable release of Ruby 3.0 | 3.0.x        | 3.16.                |
| `2.7`, `2`           | Latest stable release of Ruby 2.7 | 2.7.x        | 3.14                 |
| `2.6`                | Latest stable release of Ruby 2.6 | 2.6.x        | 3.14                 |
| `2.5`                | Latest stable release of Ruby 2.5 | 2.5.x        | 3.13                 |

The following tags are still built, but deprecated and will be removed in the future:

| Tag                      | Description                       | Ruby Version | Alpine Linux Version |
| ------------------------ | --------------------------------- | ------------ | -------------------- |
| `3-2-stable`, `3-stable` | Latest stable release of Ruby 3.2 | 3.2.x        | latest               |
| `3-1-stable`             | Latest stable release of Ruby 3.1 | 3.1.x        | latest               |
| `3-0-stable`             | Latest stable release of Ruby 3.0 | 3.0.x        | 3.16                 |
| `2-7-stable`, `2-stable` | Latest stable release of Ruby 2.7 | 2.7.x        | 3.14                 |
| `2-6-stable`             | Latest stable release of Ruby 2.6 | 2.6.x        | 3.14                 |
| `2-5-stable`             | Latest stable release of Ruby 2.5 | 2.5.x        | 3.13                 |

## Updates

The images are automatically built and pushed to the registry nightly or when changes are pushed to the main branch.
